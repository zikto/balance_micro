var hospitalData;
$(document).ready(function(){
    checkMobile();
    getHospitalData();
    var clipboard = new Clipboard('#copyCoupon');
    clipboard.on('success',function(e){
        alert('성공적으로 쿠폰번호를 복사하였습니다.');
    });
});

function checkMobile(){
    var isMobile = false; //initiate as false
// device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
    //alert(isMobile);
    if(isMobile){
        location.href = "http://balance.zikto.com/m";
    }
}

function getHospitalData(){
    $.getJSON("http://es.zikto.com/getHospitalData",function(data){
        hospitalData = data;
        makeTabs();
        makeList();
    });
}

function makeTabs()
{
    regions = [];
    regions.push("시/도");

    var i=0;
    for(i=0; i<hospitalData.length; i++){
        if(regions.indexOf(hospitalData[i].add1) == -1 )
            regions.push(hospitalData[i].add1);
    }

    var regionStr = "";
    for(i =0; i<regions.length; i++){
        regionStr+='<option value="'+regions[i]+'">'+regions[i]+'</option>'+'\r\n';
    }
    $("#region").html(regionStr);
    changeHosptial();
}

function makeList(){
    var html = "";
    for(var i =0 ; i<hospitalData.length; i++){
        html+='<tr>';
        html+='<td>'+hospitalData[i].oHName+'</td>';
        html+='<td>'+hospitalData[i].rSpecial+'</td>';
        html+='<td>'+hospitalData[i].oGPrice+'</td>';
        html+='</tr>';
    }
    $("#hospitalBody").html(html);
    $('#thetable').tableScroll({height:150});
    $('#thetable2').tableScroll();
}

$("#region").change(function(){
   changeHosptial();
});

function changeHosptial()
{
    var val = $("#region").val();
    var hospitals = [];

    if(val != "시/도"){
        var i = 0;
        for(i =0; i<hospitalData.length; i++){
            if(hospitalData[i].add1==val)
                hospitals.push(hospitalData[i]);
        }
    }else {
        for(i =0; i<hospitalData.length; i++){
            hospitals.push(hospitalData[i]);
        }
    }

    var localStr = "";
    for(i=0; i<hospitals.length; i++){
        localStr+='<option value="'+hospitals[i].id+'">'+hospitals[i].oHName+'</option>'+'\r\n';
    }
    $("#hospital").html(localStr);
}

function sendInfo(){
    ga('send', 'event', 'click', 'send_info_click');
    var email = $("#email").val();
    if(emailcheck(email)==false) {
        alert("email을 형식에 맞게 입력해 주세요.");
        return;
    }

    if($("#age").val() == ""){
        alert("나이를 입력해 주세요.");
        return;
    }

    if($("#gender").val() == ""){
        alert("나이를 입력해 주세요.");
        return;
    }

    if($("#family").val() == ""){
        alert("가족 수를 입력해 주세요.");
        return;
    }

    if($("#hospital").val() == ""){
        alert("병원 이름을 선택해 주세요.");
        return;
    }
    //alert($("#hospital").val());

    var index = $("#hospital").val();
    var hospital = findHospital(index);
    var param = {};
    param.email = email;
    param.age = $("#age").val();
    param.gender = $("#gender").val();
    param.family = $("#family").val();
    param.region = hospital.add1;
    param.hospitalName = hospital.oHName;
    var url = "http://es.zikto.com/insertBalanceMicroUser";
    $.post(url,param,function(result){
       //result IGNORE;
        joinStep(3);
    });

}


function joinStep(step){

    $("#joinStep1").css("display","none");
    $("#joinStep2").css("display","none");
    $("#joinStep3").css("display","none");
    $("#joinStep4").css("display","none");
    if(step == 1){
        $("#joinStep1").css("display","");
    }else if(step == 2){
        FB.api('/me', { locale: 'ko_KR', fields: 'name, email' },
            function(response) {
                console.log(response.email);
                $("#joinStep2").css("display","");
                $("#email").val(response.email);
            }
        );
    }else if(step == 3){
        var index = $("#hospital").val();
        var hospital = findHospital(index);
        var val = Number($("#family").val())+1;
        var nPrice = hospital.rNMPrice * val;
        var gPrice = hospital.rGMPrice * val;

        $("#nameAndHospital").text(val + "명 가족 "+hospital.oHName);
        $("#normalPrice").text("소비자가 : "+nPrice+"원");
        $("#gchPrice").text("멤버십 회원가 : "+gPrice+"원");
        $("#joinStep3").css("display","");
    }else if(step == 4){
        $("#joinStep4").css("display","");
    }
}

function findHospital(index){
    for(var i=0; i<hospitalData.length; i++){
        if(index == hospitalData[i].id){
            return hospitalData[i];
        }
    }
    return undefined;
}



function getCoupon(){
    //alert("ㅁㅉㄴㅇ");
    ga('send', 'event', 'click', 'get_coupon_click');
    FB.ui({
        method: 'share',
        name : "더 밸런스 멤버십 런칭 기념 공유 이벤",
        href: 'http://balance.zikto.com',
        picture : "http://balance.zikto.com/img/b_share.jpg",
        description : '더 밸런스 멤버십 런칭 기념 20% 세일. 직토워크 구매하고 무료로 멤버십 가입하세요. 프리미엄 건강서비스를 드립니다. [쿠폰 받는 방법] 1. 이 포스트 클릭, 직토 홈페이지로 이동. 2. 건강검진 우대 확인해보기에서 페이스북 인증 후 공유하고 쿠폰 받기',
        caption: 'balance.zikto.com'
    }, function(response){
        if (response && !response.error_message) {
            joinStep(4);
        } else {
            alert("공유 도중 에러가 발생했습니다. 다시 한 번 시도해 주세요.");
        }
    });
}

function showList(){

    var email = $("#email").val();
    if(emailcheck(email)==false) {
        alert("email을 형식에 맞게 입력해 주세요.");
        return;
    }

    if($("#age").val() == ""){
        alert("나이를 입력해 주세요.");
        return;
    }

    if($("#gender").val() == ""){
        alert("나이를 입력해 주세요.");
        return;
    }

    if($("#family").val() == ""){
        alert("가족 수를 입력해 주세요.");
        return;
    }

    if($("#hospital").val() == ""){
        alert("병원 이름을 선택해 주세요.");
        return;
    }
    //alert($("#hospital").val());

    var index = $("#hospital").val();
    var hospital = findHospital(index);
    var param = {};
    param.email = email;
    param.age = $("#age").val();
    param.gender = $("#gender").val();
    param.family = $("#family").val();
    param.region = "전체";
    param.hospitalName = "병원 리스트 확인";
    var url = "http://es.zikto.com/insertBalanceMicroUser";
    $.post(url,param,function(result){
        //result IGNORE;
        $("#joinStep1").css("display","none");
        $("#joinStep2").css("display","none");
        $("#joinStep3").css("display","none");
        $("#joinStep4").css("display","none");
        $("#hospitalList").css("visibility","visible");
    });


}

function showHospitalList(){
    $("#hospitalVipList").css("visibility","visible");
}


function goShop(){
    ga('send', 'event', 'click', 'go_shop_click');
    var url = "http://zikto.com/1/w/ko/shop_korean/";
    window.open(url);
}


$(".pe-7s-close").click(function(){
    $("#joinStep1").css("display","none");
    $("#joinStep2").css("display","none");
    $("#joinStep3").css("display","none");
    $("#joinStep4").css("display","none");
    $("#hospitalList").css("visibility","hidden");
    $("#hospitalVipList").css("visibility","hidden");
});

function loginFaceBook(){
    ga('send', 'event', 'click', 'facebook_login_click');
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            joinStep(2);
        }
        else {
            FB.login(function(response){
               if(response.status == 'connected') {
                   joinStep(2);
               }
                else {
                   alert('login 해 주세요.');
               }
            }, {scope: 'publish_actions'});
        }
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1123222341047160',
        xfbml      : true,
        version    : 'v2.7'
    });
    console.log("FaceBook Init");
};

function emailcheck(strValue)
{
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
//입력을 안했으면
    if(strValue.lenght == 0)
    {return false;}
//이메일 형식에 맞지않으면
    if (!strValue.match(regExp))
    {return false;}
    return true;
}

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "http://connect.facebook.net/ko_KR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));