var hospitalData;
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        var decode = decodeURI(results[1]);
        return decode;
    }
};


$(document).ready(function(){
    getHospitalData();
    /*var clipboard = new Clipboard('#copyCoupon');
    clipboard.on('success',function(e){
        alert('성공적으로 쿠폰번호를 복사하였습니다.');
    });*/
});

function checkFacebookLogin(callback){
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            // the user is logged in and has authenticated your
            // app, and response.authResponse supplies
            // the user's ID, a valid access token, a signed
            // request, and the time the access token
            // and signed request each expire
            var uid = response.authResponse.userID;
            //var accessToken = response.authResponse.accessToken;
            callback();
        } else {
            alert("적절한 방법으로 로그인해 주세요.");
            location.href="index.html";
        }
    });
}

function getHospitalData(){
    $.getJSON("http://es.zikto.com/getHospitalData",function(data){
        hospitalData = data;
    });
}

function makeTabs()
{
    regions = [];
    regions.push("시/도");

    var i=0;
    for(i=0; i<hospitalData.length; i++){
        if(regions.indexOf(hospitalData[i].add1) == -1 )
            regions.push(hospitalData[i].add1);
    }

    var regionStr = "";
    for(i =0; i<regions.length; i++){
        regionStr+='<option value="'+regions[i]+'">'+regions[i]+'</option>'+'\r\n';
    }
    $("#region").html(regionStr);
    changeHosptial();
}

function makeList(){
    var html = "";
    //Thead
    html+='<tr bgcolor="#545556" style="color:#fff;">';
    html+='<td align="center" width="40%" style="padding:1.0em;">병원 리스트</td>';
    html+='<td width="1px" bgcolor="#ccc"></td>';
    html+='<td align="center" width="40%"  style="padding:1.0em;">특화 항목</td>';
    html+='<td width="1px" bgcolor="#ccc"></td>';
    html+='<td align="center" width="20%"  style="padding:1.0em;">우대율</td>';
    html+='</tr>';
    for(var i =0 ; i<hospitalData.length; i++){
        html+='<tr style="font-size:1em;">';
        html+='<td align="center" width="40%"  style="padding:1.0em;">'+hospitalData[i].oHName+'</td>';
        html+='<td width="1px" bgcolor="#ccc"></td>';
        html+='<td align="center" width="40%"  style="padding:1.0em;">'+hospitalData[i].rSpecial+'</td>';
        html+='<td width="1px" bgcolor="#ccc"></td>';
        html+='<td align="center" width="20%"  style="padding:1.0em;">'+hospitalData[i].oGPrice+'</td>';
        html+='</tr>';
        html+='<tr height="1" bgcolor="#ccc">';
        html+='<td colspan="5"></td>';
        html+='</tr>';
    }
    $("#hospitalBody").html(html);
    //$('#thetable').tableScroll({height:150});
    //$('#thetable2').tableScroll();
}

function makeValue(num,index){
    var hospital = findHospital(index);
    //alert(JSON.stringify(hospital));
    var nPrice = hospital.rNMPrice * Number(num);
    console.log(nPrice);
    var gPrice = hospital.rGMPrice * Number(num);
    //13명 가족 IFC검진센터
    var str = num+"명 가족 "+hospital.oHName;
    $("#message").html(str);
    str = "소비자가: "+ nPrice +"원";
    $("#nPrice").html(str);
    str = "멤버십 회원가: " + gPrice +"원";
    $("#rPrice").html(str);
}

$("#region").change(function(){
    changeHosptial();
});

function changeHosptial()
{
    var val = $("#region").val();
    console.log(val);
    var hospitals = [];

    if(val != "시/도"){
        var i = 0;
        for(i =0; i<hospitalData.length; i++){
            if(hospitalData[i].add1==val)
                hospitals.push(hospitalData[i]);
        }
    }else {
        for(i =0; i<hospitalData.length; i++){
            hospitals.push(hospitalData[i]);
        }
    }

    var localStr = "";
    for(i=0; i<hospitals.length; i++){
        localStr+='<option value="'+hospitals[i].id+'">'+hospitals[i].oHName+'</option>'+'\r\n';
    }
    $("#hospital").html(localStr);
}

function findHospital(index){
    for(var i=0; i<hospitalData.length; i++){
        if(index == hospitalData[i].id){
            return hospitalData[i];
        }
    }
    return undefined;
}

function getCoupon(){
    //alert("ㅁㅉㄴㅇ");
    FB.ui({
        method: 'share',
        name : "더 밸런스 멤버십 런칭 기념 공유 이벤",
        href: 'http://balance.zikto.com',
        picture : "http://balance.zikto.com/img/b_share.jpg",
        description : '더 밸런스 멤버십 런칭 기념 20% 세일. 직토워크 구매하고 무료로 멤버십 가입하세요. 프리미엄 건강서비스를 드립니다. [쿠폰 받는 방법] 1. 이 포스트 클릭, 직토 홈페이지로 이동. 2. 건강검진 우대 확인해보기에서 페이스북 인증 후 공유하고 쿠폰 받기'
        ,caption: 'balance.zikto.com'
    }, function(response){
        if (response && !response.error_message) {
            location.href="coupon.html";
        } else {
            alert("공유 도중 에러가 발생했습니다. 다시 한 번 시도해 주세요.");
        }
    });
}

function loginFaceBook(callback){
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            callback(true);
        }
        else {
            FB.login(function(response){
                if(response.status == 'connected') {
                    callback(true);
                }
                else {
                    callback(false);
                }
            }, {scope: 'publish_actions'});
        }
    });
}

function goShop(){
    var url = "http://zikto.com/1/w/ko/shop_korean/";
    window.open(url);
}

function showList(){
    $("#hospitalList").css("visibility","visible");
}

function showHospitalList(){
    $("#hospitalVipList").css("visibility","visible");
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1123222341047160',
        xfbml      : true,
        version    : 'v2.7'
    });
    console.log("FaceBook Init");
    $(document).trigger('fbInit');
};

function emailcheck(strValue)
{
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
//입력을 안했으면
    if(strValue.lenght == 0)
    {return false;}
//이메일 형식에 맞지않으면
    if (!strValue.match(regExp))
    {return false;}
    return true;
}

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "http://connect.facebook.net/ko_KR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));