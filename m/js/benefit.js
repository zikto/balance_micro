$(document).ready(function () {
    $('#simple-menu').sidr({
        timing: 'ease-in-out',
        speed: 500
    });
});

$( window ).resize(function () {
    $.sidr('close', 'sidr');
});


$(document).bind('fbInit',function(){
    checkFacebookLogin(function(){
        FB.api('/me', { locale: 'ko_KR', fields: 'name, email' },
            function(response) {
                //console.log(response);
                //console.log(response.email);

                checkList();
            }
        );
    });
});

function checkList(){
    var family = $.urlParam("family");
    console.log(family);
    var id= $.urlParam("id");
    console.log(id);
    if(family == undefined || id == undefined){
        alert("잘못된 접근입니다.");
        location.href="index.html";
    }
    makeValue(family,id);
}

