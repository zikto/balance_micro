$(document).ready(function () {
    $('#simple-menu').sidr({
        timing: 'ease-in-out',
        speed: 500
    });
});

$(document).bind('fbInit',function(){
    checkFacebookLogin(function(){
        FB.api('/me', { locale: 'ko_KR', fields: 'name, email' },
            function(response) {
                //console.log(response);
                //console.log(response.email);
                $("#email").val(response.email);
                makeTabs();
            }
        );
    });
});

$( window ).resize(function () {
    $.sidr('close', 'sidr');
});

function goLogin(){
    loginFaceBook(function(response){
        if(response){
            location.href="verify_account_step2.html";
        }
        else {
            alert("login 해 주세요.");
        }
    })
}


function sendInfo(){
    ga('send', 'event', 'click', 'send_info_click');
    var email = $("#email").val();
    if(emailcheck(email)==false) {
        alert("email을 형식에 맞게 입력해 주세요.");
        return;
    }

    if($("#age").val() == ""){
        alert("나이를 입력해 주세요.");
        return;
    }

    if($("#gender").val() == ""){
        alert("성별을 입력해 주세요.");
        return;
    }

    if($("#family").val() == ""){
        alert("직계 가족 수를 입력해 주세요.");
        return;
    }

    if($("#hospital").val() == ""){
        alert("병원 이름을 선택해 주세요.");
        return;
    }
    //alert($("#hospital").val());

    var index = $("#hospital").val();
    var hospital = findHospital(index);
    var param = {};
    param.email = email;
    param.age = $("#age").val();
    param.gender = $("#gender").val();
    param.family = $("#family").val();
    param.region = hospital.add1;
    param.hospitalName = hospital.oHName;
    var url = "http://es.zikto.com/insertBalanceMicroUser";
    $.post(url,param,function(result){
        //result IGNORE;
        var retUrl ="";
        retUrl+="benefit.html";
        var familyNum = Number(param.family)+1;
        retUrl+="?family="+familyNum+"&id="+index;
        location.href=retUrl;
    });

}

function showList(){
    ga('send', 'event', 'click', 'show_list_click');
    var email = $("#email").val();
    if(emailcheck(email)==false) {
        alert("email을 형식에 맞게 입력해 주세요.");
        return;
    }

    if($("#age").val() == ""){
        alert("나이를 입력해 주세요.");
        return;
    }

    if($("#gender").val() == ""){
        alert("성별을 선택해 주세요.");
        return;
    }

    if($("#family").val() == ""){
        alert("가족 수를 입력해 주세요.");
        return;
    }

    if($("#hospital").val() == ""){
        alert("병원 이름을 선택해 주세요.");
        return;
    }
    //alert($("#hospital").val());

    var index = $("#hospital").val();
    var hospital = findHospital(index);
    var param = {};
    param.email = email;
    param.age = $("#age").val();
    param.gender = $("#gender").val();
    param.family = $("#family").val();
    param.region = "전체";
    param.hospitalName = "병원 리스트 확인";
    var url = "http://es.zikto.com/insertBalanceMicroUser";
    $.post(url,param,function(result){
        location.href="health_center_list.html";
    });


}