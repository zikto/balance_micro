$(document).ready(function () {
    $('#simple-menu').sidr({
        timing: 'ease-in-out',
        speed: 500
    });
});

$(document).bind('fbInit',function(){
    checkFacebookLogin(function(){
        FB.api('/me', { locale: 'ko_KR', fields: 'name, email' },
            function(response) {
                //console.log(response);
                //console.log(response.email);
                $("#email").val(response.email);
                makeList();
            }
        );
    });
});

function goList(){
    location.href="index.html";
}