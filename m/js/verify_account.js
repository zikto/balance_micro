$(document).ready(function () {
    $('#simple-menu').sidr({
        timing: 'ease-in-out',
        speed: 500
    });
});

function goLogin(){
    loginFaceBook(function(response){
        if(response){
            location.href="verify_account_step2.html";
        }
        else {
            alert("login 해 주세요.");
        }
    })
}

$( window ).resize(function () {
    $.sidr('close', 'sidr');
});